# Пример клиента, работающего с цепочками запросов

## Установка

1. Скачать [node.js](http://nodejs.org/) установить с путями
2. Склонировать проект
3. В папке проекта в cmd выполнить:

        npm i nodev -g
        npm i
        npm start

4. Ознакомиться с комментариями в коде

## Нюанс

Библиотека ``asker`` содержит ошибку, отправлен PR https://github.com/nodules/asker/pull/74 .
В связи с этим сразу всё не заработает. В папке ``node_modules`` замените файл
``node_modules/vow-asker/node_modules/asker/lib/error.js`` [вот этим](https://raw.github.com/summerisgone/asker/472c6b809ddb941f66f249e563c76d141bacf6c7/lib/error.js)

## Результат

На 3000 порту должен работать каталог по слагам примерно так:

[http://localhost:3000/armatura](http://localhost:3000/armatura)

![пруфпик](http://i.imgur.com/AONyAWj.png)
