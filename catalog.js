var ask = require('vow-asker');
var Vow = require('vow');
var _ = require('underscore');

var ResponseSend = 'send';
var ResponseRedirect = 'redirect';
var ResponseRender = 'render';
var rootId = '4eb8cbcb3452761cd4f10000';

var Respond = function(fn) {
    return function(req, res) {
        var promise = fn(req); // 1: СЮда приходит роутер


        promise.then(function (arr) {
            var method = arr[0],
                args = arr[1];
            res[method].apply(res, args); // 13 - в итоге рисуем шаблон
        })
        .fail(function (error) {
            res.type('text/plain');
            res.send(500, error + '\n' + error.stack || error.log());
        })
    }
};

var Get = function (url, handler) {
    var promise = Vow.promise(),
        params,
        defaults = {
            host: 'broxer.ru',
            timeout: 10000,
            maxRetries: 2
        };
    if (_.isObject(url)) {
        params = _.extend(defaults, {
            path: url.path,
            query: url.query
        })
    }
    if (_.isString(url)) {
        params = _.extend(defaults, {
            path: url
        })
    }

    ask(params)
        .then(function (response) {
            var data = JSON.parse(response.data);
            if (_.isFunction(handler)) {
                var result = handler(data);
                if (Vow.isPromise(result)) {
                    // TODO: maybe change to promise.fulfill(result) ?
                    result.then(function(valueOfResult) {
                        promise.fulfill(valueOfResult); // 11,12 - вот реализация "умного" хэндлера, см. ``if (Vow.isPromise(result))``
                    }, function(error) {
                        promise.reject(error);
                    })
                } else {
                    promise.fulfill(result);
                }
            } else {
                promise.fulfill(data);
            }

        })
        .fail(function (err) {
            promise.reject(err);
        });
    return promise; //2,4,5,6 - функция Get возвращает промис сразу же, как только вызвана. Приходим сюда несколько раз, по кол-ву запросов. В это вреям запрос ещё фактически не отправлен
};

var AfterAll = function(promises, handler) {
    var promise = Vow.promise();
    Vow.all(promises)
        .then(function() {
            promise.fulfill(handler.apply(null, arguments)); // 9 - Здесь пришли все данные, запрошенные в п.7
        }, function(error) {
            promise.reject(error);
        });

    return promise; //8 - Собственно, общий промис на все запросы принципала. Дальше приходят данные с сервера.
};

/**
 * Псевдокод метода
 *
 * module.exports.principalSync = function(req, res) {
    try {
        var slugResponse = Get('/product/getIdByslug');
        if (slugResponse.Context.status === 404) {
            res.send(404);
        }
        if (slugResponse.Context.status === 301) {
            res.redirect("/" + slugResponse.Context.slug);
        }
        if (slugResponse.Context.status === 200) {
            var principal = Get('/product/' + slugResponse.Context.Id);
            principal.Products = Get('/product/' + principal.Id + '/popularproducts');
            principal.Resellers = Get('/product/' + principal.Id + '/NavigatorResellers');
            res.render('principal', principal);
        }
    } catch (err) {
        res.send(500, error + '\n' + error.stack || error.log());
    }
}
 */
module.exports.principal = Respond(function(req) {
    return Get({
        path: '/product/getIdByslug',
        query: {slug: req.params.principal || rootId}
    }, function(slugResponse) {
        if (slugResponse.Context.status === 404) {
            return [ResponseSend, [404]];
        }
        if (slugResponse.Context.status === 301) {
            return [ResponseRedirect, ["/" + slugResponse.Context.slug]];
        }
        if (slugResponse.Context.status === 200) {
            return Get('/product/' + slugResponse.Context.Id, function(principal) { // 3 после получения слага, получаем продукт, см. if (slugResponse.Context.status === 200)
                var productsPromise = Get({
                    path: '/product/' + principal.Id + '/popularproducts',
                    query: {
                        selectSize: 5
                    }
                });
                var resellersPromise = Get({
                    path: '/product/' + principal.Id + '/NavigatorResellers',
                    query: {
                        regionCode: 74,
                        selectSize: 5
                    }
                });

                return AfterAll([productsPromise, resellersPromise], function(products, resellers) { // 7 Всё. Все запросы отправили, вернули общий промис. Ждем.
                    // TODO: fix argements pass
                    principal.Products = arguments[0][0];
                    principal.Resellers = arguments[0][1];
                    return [ResponseRender, ['principal', principal]]; // 10 - вот сработал хэндлер функции GET, который понимает, что если ему возвращается не сразу результат, а ещё один промис - надо его дождаться. Иначе Products и Resellers придут уже после отрисовки шаблона.
                })
            });
        }
    });
});
